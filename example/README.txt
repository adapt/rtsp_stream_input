To run this example,

1) install the plugin using "make install"
2) create a directory to store the output images
3) modify the test.pipe file and replace the existing video_name with one that is appropriate for your video source
4) modify the test.pipe file and replace the file_name_template with a correct path for the directory you created
5) run the test using "kwiver runner test.pipe"
6) The test will run as long as the rtsp stream is active. You will have to kill the kwiver process manually.

Note: This test saves each frame as a jpg in the test directory. This will generate a lot of data very quickly.
be prepared to kill the process quickly to avoid running out of storage space.
