/**
 * \file
 * \brief Implementation file for video input from rtsp feed.
 */

#include "rtsp_receiver.h"

#include <vital/types/timestamp.h>
#include <vital/exceptions/io.h>
#include <vital/exceptions/video.h>
#include <vital/klv/convert_metadata.h>
#include <vital/klv/misp_time.h>
#include <vital/klv/klv_data.h>
#include <vital/util/tokenize.h>
#include <vital/types/image_container.h>
#include <arrows/ocv/image_container.h>

#include <kwiversys/SystemTools.hxx>
#include <opencv2/videoio.hpp>
#include <limits>
#include <iostream>

using namespace std;

namespace kwiver {
namespace arrows {
namespace ocv {

// ==================================================================
rtsp_receiver::rtsp_receiver()
{
	// Tell Kwiver about this arrows capabilities: 
	this->set_capability(vital::algo::video_input::HAS_EOV, true);
	this->set_capability(vital::algo::video_input::HAS_FRAME_NUMBERS, true);
	this->set_capability(vital::algo::video_input::HAS_FRAME_DATA, true);
	this->set_capability(vital::algo::video_input::HAS_METADATA, false);
	this->set_capability(vital::algo::video_input::HAS_FRAME_TIME, false);
	this->set_capability(vital::algo::video_input::HAS_ABSOLUTE_FRAME_TIME, false);
	this->set_capability(vital::algo::video_input::HAS_TIMEOUT, false);
	this->set_capability(vital::algo::video_input::IS_SEEKABLE, false);
	m_running = true;
	m_framereader = nullptr;
}


rtsp_receiver::~rtsp_receiver()
{
	close();
}

// Read data as long as the m_running flag is active. This reader fills in the m_framebuffer
// usign a tripple buffering approach to ensure that the end user always has direct access to the most
// recently available frame. The tripple buffer approach has two access control mechanisms that must be protected
// from concurrent access. These 2 accessors are the m_readframe and the m_writeframe. The basic rule is that only two
// of the three buffers are active at one time. When an existing process finishes with its current buffer and needs
// the next one, it must lock the mutex, and update its index to the only buffer that is not currently being used.
// That way, the "writer" and "reader" can be fully asynchronous. The only time there is contention is when one of
// the indexes must be updated, but this lock is very short in duration, and only needs to held when someone
// is switching buffers. Reading and writing are both unimpeded.
void rtsp_receiver::reader()
{
	while( m_running )
	{
		(*m_VideoCapture) >> m_framebuffer[m_writeframe];
		if( m_framebuffer[m_writeframe].empty() )
		{
			m_running = false;
			continue;
		}
		size_t milliseconds = (*m_VideoCapture).get(cv::CAP_PROP_POS_MSEC);
		size_t frame_number = (*m_VideoCapture).get(cv::CAP_PROP_POS_FRAMES);
		m_frametimestamps[m_writeframe].set_time_seconds( milliseconds * 1000 );
		m_frametimestamps[m_writeframe].set_frame( frame_number );
		m_bufferselector.lock();
		++m_writeframe;
		if( m_writeframe >= 3 )
		{
			m_writeframe = 0;
		}
		if( m_writeframe == m_readframe )
		{
			++m_writeframe;
			if( m_writeframe >= 3 )
			{
				m_writeframe = 0;
			}
		}
		m_newframeavailable = true;
		m_bufferselector.unlock();
	}
}

// ------------------------------------------------------------------
// Get this algorithm's \link vital::config_block configuration block \endlink
vital::config_block_sptr rtsp_receiver::get_configuration() const
{
	// build a new config to return (we dont store the config block directly).
	vital::config_block_sptr config = vital::algorithm::get_configuration();
	config->set_value("username", m_username,
			"Username to authenticate access to rtsp server. Set to empty string for anonymous access" );
	config->set_value("password", m_password,
			"Password to authenticate access to rtsp server. Set to empty string for anonymous access" );
	config->set_value("address", m_rtspaddress,
			"The name or IP addess of the rtsp server. string should have \":[port]\" appended if the "
			"server uses a non-standard port." );
	return( config );
}


// ------------------------------------------------------------------
// Set this algorithm's properties via a config block
void rtsp_receiver::set_configuration(vital::config_block_sptr in_config)
{
	vital::config_block_sptr config = get_configuration();
	config->merge_config( in_config );

	m_username = config->get_value<std::string>( "username" );
	m_password = config->get_value<std::string>( "password" );
	m_rtspaddress = config->get_value<std::string>( "address" );

}


// ------------------------------------------------------------------
bool rtsp_receiver::check_configuration(vital::config_block_sptr config) const
{
	// nothing to do here, none of the values are required.
	return( true );
}


// ------------------------------------------------------------------
void rtsp_receiver::open( std::string video_name )
{
	// if there is already a video open, close it.
	close();

	m_running = true; // allow processing going forwards.

	if( video_name == "" ) // check if the user supplied an override URI for the rtsp stream.
	{
		// no url was provided to the open function, build the url from
		// the config data.
		video_name = "rtsp://" + m_username;
		if( m_username != "" )
		{
			video_name += ":";
			video_name += m_password;
			video_name += "@";
		}
		video_name += m_rtspaddress;
		video_name += "/";
	}
	else
	{
		//TODO: parse the username, password and string from the video_name
	}

	m_VideoCapture = std::unique_ptr<cv::VideoCapture>( new cv::VideoCapture(video_name.c_str()) );
	if( !m_VideoCapture || !m_VideoCapture->isOpened() )
	{
		throw kwiver::vital::video_runtime_exception("Could not open rtsp stream");
		m_running = false;
		m_VideoCapture.reset(nullptr);
		m_newframeavailable = false;
		return;
	}

	// pull the first frame into the framebuffer before proceeding. This ensures a valid read right
	// from the start.
	(*m_VideoCapture) >> m_framebuffer[0];
	m_newframeavailable = true;
	m_readframe = 2; // this will be updated to 0 or 1 when the first frame is read. 0 already has
	// good frame data in it, and if the framereader fills up 1, then the read will switch to 1 instead. Either
	// way, the readframe will always be valid from now on.
	
	m_writeframe = 1; // Tell the framereader to fill in frame 1 1st.

	size_t milliseconds = (*m_VideoCapture).get(cv::CAP_PROP_POS_MSEC);
        size_t frame_number = (*m_VideoCapture).get(cv::CAP_PROP_POS_FRAMES);
        m_frametimestamps[0].set_time_seconds( milliseconds * 1000 );
        m_frametimestamps[0].set_frame( frame_number );
	m_framereader = new std::thread( &rtsp_receiver::reader, this );
}


// ------------------------------------------------------------------
void rtsp_receiver::close()
{
	// check if the framereader still has a valid pointer. if it does, shutdown the framereader
	if( m_framereader )
	{
		m_running = false; // trigger the framereader to shutdown.
		m_framereader->join();
		delete( m_framereader );
		m_framereader = nullptr; // set the value to nullptr so that we know it is inactive.
		m_VideoCapture.reset(nullptr); // kill the opencv video stream processor.
	}
}

// ------------------------------------------------------------------
bool rtsp_receiver::next_frame( kwiver::vital::timestamp& ts, uint32_t timeout )
{
	while( !m_newframeavailable && m_running )
	{
		// wait for a new frame to arrive, or the reader to stop running.
		std::this_thread::yield();
	}
	if( !m_running )
	{
		throw kwiver::vital::video_runtime_exception("Video stream ended");	
	}
	m_bufferselector.lock();
	++m_readframe;
	if( m_readframe >= 3 )
	{
		m_readframe = 0;
	}
	if( m_readframe == m_writeframe )
	{
		++m_readframe;
		if( m_readframe >= 3 )
		{
			m_readframe =0;
		}
	}
	m_newframeavailable = false;
	m_bufferselector.unlock();
	return( true );

}

// ------------------------------------------------------------------
bool rtsp_receiver::seek_frame(kwiver::vital::timestamp& ts,
  kwiver::vital::timestamp::frame_t frame_number, uint32_t timeout)
{
	// This operation is not currently permitted
	throw kwiver::vital::video_runtime_exception("Seek operation not permitted on rtsp stream");
	return( false );
}


// ------------------------------------------------------------------
kwiver::vital::image_container_sptr rtsp_receiver::frame_image( )
{
	// build an image container. Because we are reading the stream using OCV, and saving data into
	// a container modelled afetr OCV, we can directly build the image container using the framebuffer.
	kwiver::vital::image_container_sptr img = kwiver::vital::image_container_sptr(
			new ocv::image_container(m_framebuffer[m_readframe], ocv::image_container::BGR_COLOR));
	return( img );
}


// ------------------------------------------------------------------
kwiver::vital::timestamp rtsp_receiver::frame_timestamp() const
{
	return( m_frametimestamps[m_readframe] );
}


// ------------------------------------------------------------------
kwiver::vital::metadata_vector rtsp_receiver::frame_metadata()
{
	// metadata is not currently available to rtsp streams.
	kwiver::vital::metadata_vector retval;
	auto meta = std::make_shared<kwiver::vital::metadata>();
	retval.push_back(meta);
	return( retval );
}


// ------------------------------------------------------------------
kwiver::vital::metadata_map_sptr rtsp_receiver::metadata_map()
{
	// metadata is not currently available to rtsp streams.
	kwiver::vital::metadata_map_sptr empty;
	return( empty );
}


// ------------------------------------------------------------------
bool rtsp_receiver::end_of_video() const
{
	// when the rtsp stream is interrupted, the m_running flag will be cleared
	return( !m_running );
}


// ------------------------------------------------------------------
bool rtsp_receiver::good() const
{
	// check that the framereader has advanced at least one frame since last time we used it.
	return( m_running );
}


// ------------------------------------------------------------------
bool rtsp_receiver::seekable() const
{
	// rtsp streams are not really seekable. In theory, you could; in practice its better to just avoid it.
	return( false );
}

// ------------------------------------------------------------------
size_t rtsp_receiver::num_frames() const
{
	// the number of frames in a stream is not meaningful.
	return( std::numeric_limits<size_t>::max() );
}

}}}
