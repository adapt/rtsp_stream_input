/**
 * \file
 * \brief Register RTSP algorithms implementation
 */

#include "kwiver_rtsp_receiver_plugin_export.h"
#include <vital/algo/algorithm_factory.h>

#include "rtsp_receiver.h"

namespace kwiver {
namespace arrows {
namespace ocv {

extern "C"
KWIVER_RTSP_RECEIVER_PLUGIN_EXPORT
void
register_factories( kwiver::vital::plugin_loader& vpm )
{
  static auto const module_name = std::string( "arrows.rtsp" );
  if (vpm.is_module_loaded( module_name ) )
  {
    return;
  }

  // add factory               implementation-name       type-to-create
  auto fact = vpm.ADD_ALGORITHM( "rtsp", kwiver::arrows::ocv::rtsp_receiver );
  fact->add_attribute( kwiver::vital::plugin_factory::PLUGIN_DESCRIPTION,
                       "Use opencv to read rtsp video streams as a sequence of images." )
    .add_attribute( kwiver::vital::plugin_factory::PLUGIN_MODULE_NAME, module_name )
    .add_attribute( kwiver::vital::plugin_factory::PLUGIN_VERSION, "1.0" )
    .add_attribute( kwiver::vital::plugin_factory::PLUGIN_ORGANIZATION, "Kitware Inc." )
    ;

  vpm.mark_module_as_loaded( module_name );
}

}}} 
