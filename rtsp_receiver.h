#ifndef RTSP_RECEIVER__H_
#define RTSP_RECEIVER__H_

#include "kwiver_rtsp_receiver_plugin_export.h"

#include <vital/algo/video_input.h>

#include <opencv2/opencv.hpp>
#include <mutex>
#include <memory>
#include <string>
#include <thread>
#include <chrono>

namespace kwiver {
namespace arrows {
namespace ocv {

class KWIVER_RTSP_RECEIVER_PLUGIN_EXPORT rtsp_receiver
  : public vital::algorithm_impl< rtsp_receiver, vital::algo::video_input>
{
	public:
		rtsp_receiver();
		
		virtual ~rtsp_receiver();

  
		/// Get this algorithm's \link vital::config_block configuration block \endlink
		virtual vital::config_block_sptr get_configuration() const;

		/// Set this algorithm's properties via a config block
		virtual void set_configuration(vital::config_block_sptr config);

		/// Check that the algorithm's currently configuration is valid
		virtual bool check_configuration(vital::config_block_sptr config) const;

		// The open function takes  asingle string which indicates the URI for the rtsp server.
		// The format of the rrequired string is rtsp://username:password@address/ If this string is empty,
		// the URI will be constructed from the arrow configuration information.
		virtual void open( std::string video_name );

		virtual void close();

		// returns false as long as the remote server is sending rtsp frames.
		virtual bool end_of_video() const;

		// returns true as long as the remote server is sending rtsp frames.
		virtual bool good() const;

		// returns false. rtsp streams are not seekable.
		virtual bool seekable() const;

		// returns the number of frames reported by the rtsp server. This number is meaningless for streams.
		virtual size_t num_frames() const;

		// switches the frame buffer to the most recently received frame. This will block until a new frame
		// is available.
		virtual bool next_frame( kwiver::vital::timestamp& ts,
                           uint32_t timeout = 0 );

		// This will throw an error. rtsp streams are not seekable.
		virtual bool seek_frame(kwiver::vital::timestamp& ts,
                          kwiver::vital::timestamp::frame_t frame_number,
                          uint32_t timeout = 0);

		// This will return the tiemstamp of the currently selected frame.
		virtual kwiver::vital::timestamp frame_timestamp() const;

		// This will return an image container that holds the currently selected frame.
		virtual kwiver::vital::image_container_sptr frame_image();

		// This will retun empty metadata. rtsp streams do not support metadata.
		virtual kwiver::vital::metadata_vector frame_metadata();

		// The will return an empty metadata map. rtsp streams do not support metadata.
		virtual kwiver::vital::metadata_map_sptr metadata_map();

		// The reader function will execute in its own thread and receive rtsp frames
		// storing them in a tripple buffer until they are called for by other processes.
		void reader();

	private:

		// Store a tripple buffer of frame data.
		cv::Mat m_framebuffer[3];

		// Store a tripple buffer of timestamp information
		kwiver::vital::timestamp m_frametimestamps[3];

		// indexes for the reader and writer
		size_t m_readframe;
		size_t m_writeframe;

		// the OpenCV video capture object. This object manages the rtsp stream connection and decoding
		std::unique_ptr<cv::VideoCapture> m_VideoCapture;

		// the m_running flag indicates normal operation. When processing needs to stop, the m_running flag 
		// should be set to false. 
		bool m_running;

		// Write access to the m_readframe and m_writeframe indexes must be protected for concurent use by
		// this mutex.
		std::mutex m_bufferselector;

		// The frame reader operates in a separate thread to allow asyncronous frame processing.
		std::thread *m_framereader;

		// whenever a new frame become available, this flag is set. When the most recent frame is consumed,
		// this flag is cleared.
		bool m_newframeavailable;

		// RTSP server access data
		std::string m_username;
		std::string m_password;
		std::string m_rtspaddress;
};

} } } // end namespace

#endif // RTSP_RECEIVER__H_
