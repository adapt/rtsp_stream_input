# Building #
  In a bash terminal in the project directory::
  
    cmake .
  
  
    make

# Installing #

    sudo make install
  
# Running #

    Does not run standalone. Must be used from the kwiver framework.
    See the README in the example directory for a demonstration of how to use this arrow

# License

This repository is under the Apache 2.0 license, see NOTICE and LICENSE file.
